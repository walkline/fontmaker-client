#pragma once
#include "BitFont.h"
#include "Charset.h"

class CFileMaker
{
    public:
        BOOL MakeBinFile(CBitFont* pBitFont, CCharset* pCharset, CFile* pFile, INT scan, BOOL msb, BOOL var_width, INT reversed);
};