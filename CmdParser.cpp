using namespace std;

#include "stdafx.h"
#include <afxwin.h>
#include <afxext.h>
#include <afxdisp.h>
#include <afxdtctl.h>
#include <afxcmn.h>
#include <iostream>

#include "CmdParser.h"

CCommandParser::CCommandParser(void) {}

BOOL is_valid_param(LPWSTR argv, WCHAR *param_short, WCHAR *param_long=L"")
{
	if (_wcsicmp(CString(argv).MakeLower(), param_short) == 0 ||
		_wcsicmp(CString(argv).MakeLower(), param_long) == 0) {return true;}
	return false;
}

void CCommandParser::print_params()
{
	wcout << L"\n字库参数:" << endl;
	wcout << L"      名称 : " << fontface.GetString() << endl;
	wcout << L"      大小 : " << font_size << endl;
	wcout << L"      字重 : " << (font_weight == 400 ? L"正常" : L"粗体") << endl;
	wcout << L"      斜体 : " << (font_itatlic == 0 ? L"否" : L"是") << endl;
	wcout << L"      宽度 : " << font_width << endl;
	wcout << L"      高度 : " << font_height << endl;
	wcout << L"  水平偏移 : " << font_offset_x << endl;
	wcout << L"  垂直偏移 : " << font_offset_y << endl;
	wcout << L"  扫描模式 : " << (scan_mode == 0 ? L"水平" : L"垂直") << endl;
	wcout << L"  字节顺序 : " << (byte_order == 0 ? L"低位在前" : L"高位在前") << endl;
	wcout << L"  字符反显 : " << (reverse_display == 0 ? L"否" : L"是") << endl;
	wcout << L"    索引表 : " << (has_index_table == 0 ? L"否" : L"是") << endl;
	wcout << L"  字符宽度 : " << (char_width == 0 ? L"固定" : L"可变") << endl;
	wcout << L"  输出文件 : " << output_file.GetString() << "\n" << endl;
}

void CCommandParser::parse(void)
{
	argv_list = CommandLineToArgvW(GetCommandLineW(), &args);

	if (!argv_list || args == 1) {
		print_help();
		exit(0);
	}

	for (int index = 1; index < args; index++) {
		if (is_valid_param(argv_list[index], L"-f", L"--face")) {
			fontface = CString(argv_list[index + 1]).Trim(L"'");
			index++;
		} else if (is_valid_param(argv_list[index], L"-s", L"--size")) {
			font_size = _wtoi(argv_list[index + 1]);
			font_height = font_size;
			index++;
		} else if (is_valid_param(argv_list[index], L"-w", L"--width")) {
			font_width = _wtoi(argv_list[index + 1]);
			index++;
		} else if (is_valid_param(argv_list[index], L"-n", L"--normall")) {
			font_weight = 400;
		} else if (is_valid_param(argv_list[index], L"-b", L"--bold")) {
			font_weight = 700;
		} else if (is_valid_param(argv_list[index], L"-i", L"--italic")) {
			font_itatlic = 1;
		} else if (is_valid_param(argv_list[index], L"-x", L"--offset-x")) {
			font_offset_x = _wtoi(argv_list[index + 1]);
			index++;
		} else if (is_valid_param(argv_list[index], L"-y", L"--offset-y")) {
			font_offset_y = _wtoi(argv_list[index + 1]);
			index++;
		} else if (is_valid_param(argv_list[index], L"-sh", L"--scan-horizontal")) {
			scan_mode = SCAN_MODE_HORIZONTAL;
		} else if (is_valid_param(argv_list[index], L"-sv", L"--scan-vertical")) {
			scan_mode = SCAN_MODE_VERTICAL;
		} else if (is_valid_param(argv_list[index], L"-m", L"--msb")) {
			byte_order = BYTEORDER_MSB;
		} else if (is_valid_param(argv_list[index], L"-l", L"--lsb")) {
			byte_order = BYTEORDER_LSB;
		} else if (is_valid_param(argv_list[index], L"-rd", L"--reverse-display")) {
			reverse_display = 1;
		} else if (is_valid_param(argv_list[index], L"-o", L"--output")) {
			output_file = CString(argv_list[index + 1]);
			index++;
		} else if (is_valid_param(argv_list[index], L"", L"--input")) {
			input_customized_file = CString(argv_list[index + 1]);
			using_customized_file = 1;
			index++;
		} else if (is_valid_param(argv_list[index], L"", L"--charset")) {
			input_customized_cst = CString(argv_list[index + 1]);
			using_customized_cst = 1;
			index++;
		} else if (is_valid_param(argv_list[index], L"", L"--version")) {
			wcout << "Font Maker Client " << app_version.GetString() << endl;
			exit(0);
		}
	}

	if (font_width == 0) {font_width = font_size;}
	if (font_height == 0) {font_height = font_size;}

	LocalFree(argv_list);

	if (fontface.GetLength() == 0) {
		wcout << L"字体文件不能为空！" << endl;
		exit(1);
	}

	if (scan_mode == SCAN_MODE_VERTICAL && byte_order == BYTEORDER_MSB) {
		wcout << L"不支持 VMSB 模式！" << endl;
		exit(1);
	}
}

void CCommandParser::print_help(void) {
	char filename[MAX_PATH + 1];
	strcpy_s(filename, CT2A(get_exe_filename().GetBuffer()));

	wcout << "Usage: " << filename << " [OPTIONS]" << endl;
	wcout << "" << endl;
	wcout << "Options:" << endl;
	wcout << "    -f, --face FONT_FACE"			<< L"	字体名称。如“幼圆”，“Consolas”。[必须的]\n" << endl;
	wcout << "    -s, --size FONT_SIZE"			<< L"	字体大小。如“16”，“24”。[默认值：16]\n" << endl;
	wcout << "    -w, --width CHAR_WIDTH"		<< L"	字符宽度。如“16”，“24”。[默认值：FONT_SIZE]\n" << endl;
	wcout << "    -n, --normal"				<< L"		字体字重为：普通。[默认值]\n" << endl;
	wcout << "    -b, --bold"			<< L"			字体字重为：加粗\n" << endl;
	wcout << "    -i, --italic"				<< L"		字体显示为斜体\n" << endl;
	wcout << "    -x, --offset-x OFFSETX"		<< L"	字符水平偏移量。如“3”，“-5”。[默认值：0]\n" << endl;
	wcout << "    -y, --offset-y OFFSETY"		<< L"	字符垂直偏移量。如“-2”，“3”。[默认值：0]\n" << endl;
	wcout << "    -sv --scan-vertical"		<< L"		垂直方式扫描字符矩阵。[默认值]\n" << endl;
	wcout << "    -sh --scan-horizontal"		<< L"	水平方式扫描字符矩阵。\n" << endl;
	wcout << "    -l, --lsb" << L"			字节低位在前。[默认值]\n" << endl;
	wcout << "    -m, --msb" << L"			字节高位在前。\n" << endl;
	wcout << "    -rd, --reverse-display" << L"      字符反显。\n" << endl;
	wcout << "    --input FILENAME" << L"		指定输入文本，生成自定义字库。\n" << endl;
	wcout << "    --charset FILENAME" << L"		指定字符集文件，生成自定义字库。\n" << endl;
	wcout << "    -o, --output FILENAME"		<< L"	指定生成文件的文件名。[默认值：combined.bin]\n" << endl;
	wcout << "\nDisabled Options:" << endl;
	wcout << "    -h, --height CHAR_HEIGHT" << L"	字符高度。如“16”，“24”。[禁用项，默认值：FONT_SIZE]\n" << endl;
	wcout << "    -d, --fixed-width" << L"		字符宽度固定。[禁用项，默认值]\n" << endl;
	wcout << "    -v, --variable-width" << L"	字符宽度可变。[禁用项]\n" << endl;
	wcout << "    -t, --include-table" << L"		包含 GB2312 索引表。[禁用项，默认值]\n" << endl;
	wcout << "\nSamples:" << endl;
	wcout << "    " << filename << L" -f 幼圆" << L"				使用默认参数值生成幼圆字体的字库\n" << endl;
	wcout << "    " << filename << L" -f \"微软雅黑\" -s 24 -o msyh.bin" << L"	生成微软雅黑字体，字号 24，普通字重，垂直扫描，" << endl;
	wcout << L"							字节低位在前，文件名 msyh.bin 的字库文件\n" << endl;
	wcout << "    " << filename << L" -f 幼圆 --input novel.txt" << L"		根据 novel.txt 中的文字生成自定义字库" << endl;
}

CString CCommandParser::get_exe_filename(void) {
	TCHAR full_path[MAX_PATH + 1];
	CString csFileFullName;

	GetModuleFileName(NULL, full_path, MAX_PATH);

	csFileFullName = full_path;
	int nPos = csFileFullName.ReverseFind('\\');
	
	csFileFullName = csFileFullName.Right(csFileFullName.GetLength() - nPos - 1);
	return csFileFullName;
}
