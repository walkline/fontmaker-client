#include "StdAfx.h"
#include "FileMaker.h"

static const char hex_encode(int ch)
{
	static const char hex_code[]={"0123456789ABCDEF"};
	return hex_code[ch];
}

static int bin_to_hex(char* output, void* input, int len)
{
	int i;
	char* start;
	char* pch;
	start = output;
	pch = (char*)input;

	for(i=0; i<len; i++) {
		*output++ = '0';
		*output++ = 'x';
		*output++ = hex_encode((*pch>>4)&0x0F);
		*output++ = hex_encode((*pch)&0x0F);
		*output++ = ',';

		if((i&0xF)==0xF) {
			*output++ = '\r';
			*output++ = '\n';
		}

		pch++;
	}

	*output = 0;
	return (int)(output-start);
}

static int UnicodeToUTF8(const WCHAR* input, char* output, int size)
{
    memset(output, 0, size);
    return WideCharToMultiByte(CP_UTF8, 0, input, 1, output, size, NULL, NULL);		
}

BOOL CFileMaker::MakeBinFile(CBitFont* pBitFont, CCharset* pCharset, CFile* pFile, INT scan, BOOL msb, BOOL var_width, INT reversed)
{
	INT i;
	INT count;
	INT size;
	WCHAR ch;
	BYTE* bits;
    UINT  bits_size;
	bits_size = pBitFont->GetBits(NULL,0,scan,msb,var_width, reversed);
	bits = (BYTE*)malloc(bits_size);
	if(bits==NULL) {return FALSE;}

	count = pCharset->GetCharCount();

	for(i=0;i<count;i++) {
		ch = pCharset->GetChar(i);
		pBitFont->PaintChar(ch);
		size = pBitFont->GetBits(bits,bits_size,scan,msb,var_width, reversed);
		pFile->Write(bits,size);
	}

	free(bits);
	return TRUE;
}