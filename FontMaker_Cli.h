#pragma once

#include "stdafx.h"
#include <afxwin.h>
#include <afxext.h>
#include <afxdisp.h>
#include <afxdtctl.h>
#include <afxcmn.h>

#include "Charset.h"
#include "BitFont.h"
#include "FileMaker.h"
#include "CmdParser.h"

typedef struct tagFontLibHeader {
	BYTE magic[4];
	DWORD file_size;
	BYTE font_width;
	BYTE font_height;
	WORD char_count;
	BYTE has_index_table;
	BYTE scan_mode;
	BYTE byte_order;
	DWORD ascii_start;
	DWORD gb2312_start;
	char reserved[2];
} FL_Header, * PFL_Header;

INT FL_Header_Size = 25;
CBitFont m_bitfont;
CCharset m_charset;
CFileMaker maker;
HFONT m_hFont;
LOGFONT log_font;
CCommandParser m_cmd_parser;
UINT m_character_count;

CString ascii_output_filename = L"~ASCII.bin";
CString gb2312_output_filename = L"~GB2312.bin";
CString customized_output_filename = L"~CUSTOMIZED.bin";
CString customized_index_table_filename = L"~CUSTOMIZED.cst";
