<h1 align="center">FontMaker Client</h1>

<p align="center"><img src="https://img.shields.io/badge/Licence-MIT-green.svg?style=for-the-badge" /></p>

### 项目介绍

> 主要功能代码来自于 [FontMaker](https://gitee.com/kerndev/FontMaker) 项目

以命令行方式生成适合 [MicroPython New FontLib](https://gitee.com/walkline/micropython-new-fontlib) 项目使用的字库

### 字库说明

> 使用`v1.0.0`之后版本生成的字库与之前版本的字库不兼容

本项目生成的字库与其它字库的区别如下：

* 文件头，包含字库文件相关信息
* 字符尺寸固定，例如选择`字体大小`为 16 像素，那么字符点阵的宽和高都为 16 像素
* 扫描方式为水平扫描（垂直扫描方式获取的数据有问题，所以暂不支持）
* 字节顺序支持`MSB`和`LSB`（但垂直扫描方式不支持`MSB`）
* 包含`GB2312`索引表，暂不支持外部索引表文件
* 支持使用文本文件生成自定义字库

### 获取可执行文件

可以自行编译代码，或者从 [binary](https://gitee.com/walkline/fontmaker-client/tree/binary/) 分支下载

要查看版本号可使用如下命令：

```bash
$ FontMaker_Cli.exe --version
```

### 如何生成字库

最简单的方法是只指定`字体名称`，其它参数使用默认值

```bash
$ FontMaker_Cli.exe -f "幼圆"
```

这样得到的字库文件属性如下：

* 文件名：`combined.bin`
* 字体名称：`幼圆`
* 字体大小：`16 像素`
* 字体字重：`普通`
* 水平偏移：`0 像素`
* 垂直偏移：`0 像素`
* 点阵宽度：`16 像素`
* 点阵高度：`16 像素`
* 字符宽度：`固定宽度`
* 字节顺序：`低字节在前`
* 扫描方式：`垂直扫描`
* 包含`GB2312 索引表`

如果要生成`微软雅黑`、`大小 24`、`垂直扫描`、`字节高位在前`、`文件名 yahei_24_hmsb.bin`的字库文件，则使用如下命令：

```bash
$ FontMaker_Cli.exe -f "微软雅黑" -s 24 -sh -m -o yahei_24_hmsb.bin
```

如果要生成自定义字库，则使用如下命令：

```bash
$ FontMaker_Cli.exe -f "微软雅黑" --input filename.txt -o customized.bin
```

还可以使用 [FontMaker Client GUI](https://gitee.com/walkline/fontmaker-client-gui) 这个简易窗口程序生成字库

### 如何使用字库

具体使用方法参考 [fontlib.py](https://gitee.com/walkline/micropython-new-fontlib/blob/master/fontlib.py) 相关代码

### 命令行参数

> 带`-`的为短命令，带`--`的为长命令，二选一使用
>
> 参数出现冲突时，如同时选择`-l`和`-m`，则以后出现者为准

* 必选参数
	* **-f**, **--face FONTFACE**：指定字体名称

* 可选参数：字形相关
	* **-s**, **--size FONTSIZE**：字体大小，**默认值：16**
	* **-n**, **--normal**：字体字重为`普通`，**默认选择**
	* **-b**, **--bold**：字体字重为`加粗`
	* **-i**, **--italic**：字体为`斜体`

* 可选参数：字符点阵相关
	* **-w**, **--width CHARWIDTH**：字符点阵宽度，**默认值：FONT_SIZE**
	* **-x**, **--offset-x OFFSETX**：水平偏移量，**默认值：0**
	* **-y**, **--offset-y OFFSETY**：垂直偏移量，**默认值：0**
	* **-sv**, **--scan-vertical**：垂直方式扫描，**默认选择**
	* **-sh**, **--scan-horizontal**：水平方式扫描
	* **-l**, **--lsb**：字节低位在前，**默认选择**
	* **-m**, **--msb**：字节高位在前
	* **-rd**, **--reverse-display**：字符反显

* 可选参数：文件相关
	* **--charset FILENAME**：指定字符集文件，生成自定义字库
	* **--input FILENAME**：使用文本文件生成自定义字库文件
	* **-o**, **--output FILENAME**：生成字库文件的文件名，**默认值：combined.bin**

* 不可选参数
	* **-h**, **--height CHAR_HEIGHT**：字符点阵高度，**默认值：FONT_SIZE**
	* **-d**, **--fixed-width**：字符点阵宽度固定，**默认选择**
	* **-v**, **--variable-width**：字符点阵宽度可变
	* **-t**, **--include-table**：包含`GB2312`索引表，**默认选择**

### 附录：字库结构

字库结构分为 4 个部分：

* 文件头
* `GB2312`索引表
* `ASCII`字符数据
* `GB2312`字符数据

#### 文件头

```c++
typedef struct tagFontLibHeader {
    BYTE magic[4];
    DWORD file_size;
    BYTE font_width;
    BYTE font_height;
    WORD char_count;
    BYTE has_index_table;
    BYTE scan_mode;
    BYTE byte_order;
    DWORD ascii_start;
    DWORD gb2312_start;
    char reserved[2];
} FL_Header, * PFL_Header;
```

* `[4] magic`：文件识别代码，默认为`FMUX`，自定义字库为`FMUY`
* `[4] file_size`：字库文件大小
* `[1] font_width`：字体宽度
* `[1] font_height`：字体高度
* `[2] char_count`：包含字符总数
* `[1] has_index_table`：是否包含`GB2312`索引表
* `[1] scan_mode`：扫描方式
* `[1] byte_order`：字节顺序
* `[4] ascii_start`：`ASCII`字符数据起始地址
* `[4] gb2312_start`：`GB2312`或`自定义`字符起始地址
* `[2] reserved`：保留内容

#### `GB2312`索引表

因为`MicroPython`仅支持`Unicode`编码，且`Unicode`与`GB2312`无直接转换关系，所以必须使用一个索引表进行转换查询

#### `ASCII`、`GB2312` 字符数据

根据索引表顺序排列的字符点阵数据，每个字符都为固定尺寸，字符数据大小计算公式为：

```python
((font_width - 1) // 8 + 1) * font_height
```

### 资源版权声明

`/fonts/emoticons21.fon` 文件来自于 [u8g2](https://github.com/olikraus/u8g2/tree/master/tools/font/emoticons) 项目
`/fonts/open-iconic.ttf` 文件来自于 [Open Iconic](https://github.com/iconic/open-iconic) 项目

### 合作及交流

* 联系邮箱：<walkline@163.com>
* QQ 交流群：
	* 走线物联：[163271910](https://jq.qq.com/?_wv=1027&k=VlT7Bjs9)
	* 扇贝物联：[31324057](https://jq.qq.com/?_wv=1027&k=IQh2OLw9)

<p align="center"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_walkline.png" width="300px" alt="走线物联"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_bigiot.png" width="300px" alt="扇贝物联"></p>
