﻿font_list = [
	['open-iconic', range(0xe000, 0xe0de + 1)],
	['emoticons21', range(0x20, 0x3f + 1)],
	['icomoon-free', range(0xe900, 0xeaea + 1)]
]

def write_cst_file(filename, range):
	with open(filename + '.cst', 'wb') as file:
		for char in range:
			if isinstance(char, str):
				char = ord(char)

			print(chr(char), end='')
			file.write(char.to_bytes(2,'little'))
	
	print(f'\n\n{filename + ".cst"} generated')

if __name__ == '__main__':
	print('Font File List:')
	for index, font in enumerate(font_list, start=1):
		print(f'    [{index}] {font[0]}')

	selected = None
	while True:
		try:
			selected=int(input('Choose a font: '))
			print('')
			assert type(selected) is int and 0 < selected <= len(font_list)
			break
		except KeyboardInterrupt:
			break
		except:
			pass

	if selected:
		write_cst_file(font_list[selected - 1][0], font_list[selected - 1][1])
