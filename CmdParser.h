#pragma once
using namespace std;

#include "stdafx.h"
#include <afxwin.h>
#include <afxext.h>
#include <afxdisp.h>
#include <afxdtctl.h>
#include <afxcmn.h>

constexpr auto SCAN_MODE_HORIZONTAL = 0;;
constexpr auto SCAN_MODE_VERTICAL = 1;;
constexpr auto BYTEORDER_MSB = 1;;
constexpr auto BYTEORDER_LSB = 0;;
constexpr auto CHAR_WIDTH_FIXED = 0;;
constexpr auto CHAR_WIDTH_VARIABLE = 1;;

const CString app_version = L"v1.0.3";

class CCommandParser
{
	public:
		CCommandParser(void);
		void parse(void);
		void print_help(void);
		void print_params(void);

	public:
		CString fontface;
		CString input_customized_file;
		CString input_customized_cst;
		CString output_file = L"combined.bin";
		INT font_weight = 400;
		INT font_itatlic = 0;
		INT font_size = 16;
		INT font_width = 0;
		INT font_height = 0;
		INT font_offset_x = 0;
		INT font_offset_y = 0;
		INT scan_mode = SCAN_MODE_VERTICAL;
		INT byte_order = BYTEORDER_LSB;
		INT reverse_display = 0;
		INT has_index_table = 1;
		INT char_width = CHAR_WIDTH_FIXED;
		INT using_customized_file = 0;
		INT using_customized_cst = 0;

	private:
		CString get_exe_filename(void);

	private:
		LPWSTR* argv_list = {};
		int args = 0;
};